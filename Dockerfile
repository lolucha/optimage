FROM debian:buster-slim
RUN apt-get update && apt-get install -y --no-install-recommends git inotify-tools jpegoptim optipng nodejs npm tar wget webp imagemagick

#enable golang
RUN wget https://dl.google.com/go/go1.15.6.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go1.15.6.linux-amd64.tar.gz && \
    rm go1.15.6.linux-amd64.tar.gz
ENV PATH $PATH:/usr/local/go/bin

#instal sqip
RUN go get -u github.com/fogleman/primitive && \
    npm install -g sqip && \
    npm install --save lqip

RUN apt-get purge -y --allow-remove-essential npm wget git && rm -rf /var/lib/apt/lists/*

RUN mkdir /cache
ENV SQIP_EXTENSION ._lazy.svg
ENV ENABLE_OPTIPNG 1
ENV ENABLE_JPEGOPTIM 1
ENV ENABLE_SQIP 1
ENV ENABLE_WATCHER 1
ENV ENABLE_WEBP 1
ENV WEBP_QUALITY 75
ENV ENABLE_LQIP 1
ENV LQIP_EXTENSION .base64
ENV TARGET_SUBDIRECTORY optimg
ENV CONVERT_PNG_TO_JPG 1

COPY ./src/* /

CMD ./entrypoint.sh
