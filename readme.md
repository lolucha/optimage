# Purpose

Project aims to automatically optimise images for web purposes. Images are optimised
and optionally [sqip](https://github.com/axe312ger/sqip/tree/legacy#readme) 
placeholders are created.

## How it works

Docker container watch folders added as volumes under ```/data``` 
directory and for each new/updated image executes defined actions like image
optimisation or creating placeholder.

## Supported file extensions:
* jpg
* png

## Supported actions
* jpg optimisation using jpegoptim
* png optimisation using optipng
* creating [SQIP](https://github.com/axe312ger/sqip/tree/legacy#readme) placeholders
* creating [LQIP](https://github.com/zouhir/lqip) placeholders (base64 encoded)
* creating webp
* creating jpg version of png file


## Available ENV with default values 
* ```ENV SQIP_EXTENSION ._lazy.svg``` - extension added to SQIP placeholders 
* ```ENV ENABLE_OPTIPNG 1``` - enable/disable png optimisation
* ```ENV ENABLE_JPEGOPTIM 1``` - enable/disable jpeg optimisation
* ```ENV ENABLE_SQIP 1``` - enable/disable creation of sqip placeholders 
* ```ENV ENABLE_WATCHER 1``` - enable/disable file watcher on startup
* ```ENV ENABLE_WEBP 1``` - enable/disable webp generation
* ```ENV WEBP_QUALITY 75``` - webp file quality
* ```ENV ENABLE_LQIP 1``` - enable/disable LQIP placeholders generator 
* ```ENV LQIP_EXTENSION .base64``` - extension added to LQIP placeholders
* ```ENV TARGET_SUBDIRECTORY optimg``` - subdirectory for generated files
* ```ENV CONVERT_PNG_TO_JPG 1``` - enable/disable generation of jpg for png files

## USAGE

Run the container using docker run or docker compose - see example configuration: 
[example docker-compose.yml](example-docker-compose.yml)

Container watches for changes in all volumes mounted under ```/data``` directory.
If any file is saved/moved to the watched directory actions based on
passed environment variables are executed

Optionally you can trigger an action on all files (ie on first run). Simply execute:

```docker-compose exec <service-name> /process_all.sh```

## Disabling/enabling file watcher

Sometimes you need to disable/enable file watcher. I.e. You might want
to disable file watcher during downloading large amount of media from backup.

You can disable file watcher using env variable but it demands container restart.
You can use provided scripts instead.

Keep in mind that after container restart it will be set according to env variables
so scripts does not change watcher status permanently but work only during runtime.

```bash
docker-compose exec <service-name> /enable_watcher.sh

or

docker-compose exec <service-name> /disable_watcher.sh
```
