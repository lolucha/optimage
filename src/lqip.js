const lqip = require('lqip');

const source = process.argv[2];
const target = process.argv[3];

lqip.base64(source).then(res => {
    console.log(res); // "data:image/jpeg;base64,/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhY.....
});
