#!/bin/bash
result=0
function get_lowercase_extension() {
    extension="${1##*.}"
    echo "$extension" | tr '[:upper:]' '[:lower:]'
}
file=$1
path="$(dirname "${1}")"
file_type=$(get_lowercase_extension "$file")

#ignore temp files
[ "$file_type" = "tmp" ] && echo 0 && exit 1

# ignore files we just created
[[ "$path" == *"${TARGET_SUBDIRECTORY}" ]] && echo 0 && exit 1

#ignoer if directory
[ ! -f "${file}" ] && echo 0 && exit 1

allowed_extensions=("jpg" "jpeg" "png")

# ignore if not supported extension
[[ ! ${allowed_extensions[*]} =~ "$file_type" ]] && echo 0 && exit 1

echo 1
