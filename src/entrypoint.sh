#!/bin/bash
rm -f /watcher.lock
[ "$ENABLE_WATCHER" -eq 0 ] && touch watcher.lock && echo Watcher temporarily disabled with lock file - remove /watcher.lock to enable watcher
./watch.sh
