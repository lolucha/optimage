#!/bin/bash
TARGET=/data

function get_lowercase_extension() {
    extension="${1##*.}"
    echo "$extension" | tr '[:upper:]' '[:lower:]'
}

inotifywait -m -r -e create -e moved_to --format "%w %f" $TARGET | while read path file
  do
    [ -f "/watcher.lock" ] && echo ----- File watcher disabled ---- && continue
    ./process_file.sh ${path}${file}
  done
