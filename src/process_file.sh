#!/bin/bash

function get_lowercase_extension() {
    extension="${1##*.}"
    echo "$extension" | tr '[:upper:]' '[:lower:]'
}

path="$(dirname "${1}")"
source_file="$(basename "${1}")"
source_file_path=${path}/${source_file}
source_file_type=$(get_lowercase_extension "$source_file");
jpeg_extensions=("jpg" "jpeg")

function get_target_directory() {
  # check if we are not creating subdirectories recursively
  if [[ "$path" == *${TARGET_SUBDIRECTORY} ]]; then
    echo $path
  else
    echo ${path}/${TARGET_SUBDIRECTORY}
  fi
}

function get_base_target_file_path() {
    echo "$(get_target_directory)"/${source_file}
}

function create_target_directory_if_not_exists() {
  dir="$(get_target_directory)"
  parentdir="$(dirname "$dir")"
  [ ! -d "/path/to/dir" ] && mkdir -p $dir && chmod --reference=${parentdir} $dir && chown --reference=${parentdir} $dir
}

function optimize_jpg () {
  echo Optimising $source_file_path

  if jpegoptim -P -s -o -d "$(get_target_directory)" $source_file_path | grep -q ', skipped.'; then
    # if original file does not require optimalization need to copy it manually
    cp $source_file_path "$(get_base_target_file_path)"
  fi
}

function optimize_png() {
  echo Optimising $source_file_path
  rm $(get_base_target_file_path)
  optipng -dir "$(get_target_directory)" -preserve -force $source_file_path
}

function convert_png_to_jpg() {
  echo Converting $source_file_path to jpg
  target_file_path="$(get_base_target_file_path)".jpg
  convert $source_file_path $target_file_path
  jpegoptim -s $target_file_path
  chmod --reference=${source_file_path} $target_file_path && chown --reference=${source_file_path} $target_file_path
}

function convert_to_webp() {
  echo Converting to webp
  webp_file="$(get_base_target_file_path)".webp
  cwebp $source_file_path -q $WEBP_QUALITY -o $webp_file
  chmod --reference=${source_file_path} $webp_file && chown --reference=${source_file_path} $webp_file
}

function convert_to_sqip() {
  target_file="$(get_base_target_file_path)"${SQIP_EXTENSION}
  echo Creating SQIP $target_file
  sqip -o $target_file $source_file_path
  chmod --reference=${source_file_path} $target_file && chown --reference=${source_file_path} $target_file
}

function convert_to_lqip() {
  lqip_file=$(get_base_target_file_path)${LQIP_EXTENSION}
  node ./lqip.js $source_file_path > $lqip_file
  chmod --reference=${source_file_path} $lqip_file && chown --reference=${source_file_path} $lqip_file
}

# terminate if file cant be processed
can_be_processed=$(./file_checker.sh "$source_file_path")
[ "$can_be_processed" = "0" ] && echo -- Ignoring ${source_file_path} && exit 0

create_target_directory_if_not_exists

echo Starting processing file $source_file_path
if [ $ENABLE_JPEGOPTIM -eq 1 ] && [[ " ${jpeg_extensions[@]} " =~ " ${source_file_type} " ]]; then
  optimize_jpg
fi

# optimize png files
if [ $ENABLE_OPTIPNG -eq 1 ] && [[ "$source_file_type" = "png" ]]; then
  optimize_png
fi

if [ $CONVERT_PNG_TO_JPG -eq 1 ] && [[ "$source_file_type" = "png" ]]; then
  convert_png_to_jpg
fi

if [ $ENABLE_WEBP -eq 1 ]; then
  convert_to_webp
fi

if [ $ENABLE_SQIP -eq 1 ]; then
  convert_to_sqip
fi

if [ $ENABLE_LQIP -eq 1 ]; then
  convert_to_lqip
fi

echo DONE !!!
